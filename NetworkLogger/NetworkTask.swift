//
//  NetworkTask.swift
//  NetworkLogger
//
//  Created by Сергей Лукичев on 21.06.2023.
//

import Foundation

class NetworkTask: Hashable {
    
    static func == (lhs: NetworkTask, rhs: NetworkTask) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(guid)
        hasher.combine(status)
    }
    
    let guid: Int
    var request: Request
    var response: Response?
    var status: Status
    
    init(guid: Int, request: NetworkTask.Request, response: NetworkTask.Response? = nil, status: NetworkTask.Status = .pending) {
        self.guid = guid
        self.request = request
        self.response = response
        self.status = status
    }
}

extension NetworkTask {
    class Request {
        let createTime: Date
        let url: String
        let httpMethod: String
        let headers: [String : String]?
        let body: Data?
        
        init(createTime: Date, url: String, httpMethod: String, headers: [String : String]?, body: Data?) {
            self.createTime = createTime
            self.url = url
            self.httpMethod = httpMethod
            self.headers = headers
            self.body = body
        }
    }
    
    class Response {
        let createTime: Date
        let headers: [String : String]?
        let body: Data?
        let statusCode: Int

        init(createTime: Date, headers: [String : String]?, body: Data?, statusCode: Int) {
            self.createTime = createTime
            self.headers = headers
            self.body = body
            self.statusCode = statusCode
        }
    }
    
    enum Status: String {
        case ok
        case error
        case unauthorized
        case cancelled
        case pending
        case downloading
    }
}
