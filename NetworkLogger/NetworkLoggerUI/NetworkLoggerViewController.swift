//
//  NetworkLoggerViewController.swift
//  NetworkLogger
//
//  Created by macBook on 21.06.2023.
//  
//

import UIKit

// MARK: Protocol - NetworkLoggerPresenterToViewProtocol (Presenter -> View)
protocol NetworkLoggerPresenterToViewProtocol: AnyObject {

}

// MARK: Protocol - NetworkLoggerRouterToViewProtocol (Router -> View)
protocol NetworkLoggerRouterToViewProtocol: AnyObject {
    func presentView(view: UIViewController)
    func pushView(view: UIViewController)
}

class NetworkLoggerViewController: UIViewController {
    
    enum Section: CaseIterable {
        case firstSection
        case secondSection
        case thirdSection
    }
    
    // MARK: - Property
    var presenter: NetworkLoggerViewToPresenterProtocol!
    
    private lazy var storage: NetworkStorage = {
        let storage = NetworkStorage()
        storage.delegate = self
        return storage
    }()
    
   // let loggerList: [NetworkTask] = [NetworkTask(request: NetworkTask.Request(url: "ddd", httpMethod: "ddd", headers: ["dd":"dd"], body: ["dd":"dd"]))]
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.register(LoggerTableViewCell.self, forCellReuseIdentifier: LoggerTableViewCell.reuseIdentifier)
        tableView.keyboardDismissMode = .onDrag
        tableView.backgroundColor = .gray
        tableView.sectionHeaderHeight = 12
        tableView.sectionFooterHeight = 1
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.allowsSelectionDuringEditing = true
        
        return tableView
    }()
    
    private lazy var dataSource = UITableViewDiffableDataSource<Section, NetworkTask>(tableView: tableView) { [weak self] tableView, indexPath, currentItem in
        guard let self = self,
              let cell = tableView.dequeueReusableCell(withIdentifier: LoggerTableViewCell.reuseIdentifier, for: indexPath) as? LoggerTableViewCell
        else {
            return UITableViewCell(style: .default, reuseIdentifier: nil)
        }
        
        cell.configuration(withItemModel: currentItem)
        
        return cell
    }

    // MARK: - init
    init() {
        super.init(nibName: nil, bundle: nil)

        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        commonInit()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
        presenter.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = dataSource
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        upadeTable()
    }
    
    // MARK: - private func
    private func commonInit() {

    }

    private func configureUI() {

    }
    
    func upadeTable() {
        var snapshot = NSDiffableDataSourceSnapshot<Section, NetworkTask>()
        snapshot.appendSections([.firstSection])
        snapshot.appendItems(storage.tasks, toSection: Section.firstSection)
        dataSource.apply(snapshot, animatingDifferences: false)
    }
}

// MARK: Extension - NetworkLoggerPresenterToViewProtocol 
extension NetworkLoggerViewController: NetworkLoggerPresenterToViewProtocol{
    
}

// MARK: Extension - NetworkLoggerRouterToViewProtocol
extension NetworkLoggerViewController: NetworkLoggerRouterToViewProtocol{
    func presentView(view: UIViewController) {
        present(view, animated: true, completion: nil)
    }

    func pushView(view: UIViewController) {
        navigationController?.pushViewController(view, animated: true)
    }
}

extension NetworkLoggerViewController: UITableViewDelegate {
    
}

extension NetworkLoggerViewController: NetworkStorageDelegate {
    func storageIsUpdated() {
        upadeTable()
    }
}
