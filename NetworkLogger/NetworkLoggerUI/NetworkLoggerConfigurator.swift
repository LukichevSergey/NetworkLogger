//
//  NetworkLoggerConfigurator.swift
//  NetworkLogger
//
//  Created by macBook on 21.06.2023.
//  
//

import UIKit

class NetworkLoggerConfigurator {
    func configure() -> UIViewController {
        let view = NetworkLoggerViewController()
        let presenter = NetworkLoggerPresenter()
        let router = NetworkLoggerRouter()
        let interactor = NetworkLoggerInteractor()
        
        view.presenter = presenter

        presenter.router = router
        presenter.interactor = interactor
        presenter.view = view

        interactor.presenter = presenter
        
        router.view = view
        
        return view
    }
}