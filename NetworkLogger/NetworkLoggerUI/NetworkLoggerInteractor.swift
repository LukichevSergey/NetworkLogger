//
//  NetworkLoggerInteractor.swift
//  NetworkLogger
//
//  Created by macBook on 21.06.2023.
//  
//

import Foundation

// MARK: Protocol - NetworkLoggerPresenterToInteractorProtocol (Presenter -> Interactor)
protocol NetworkLoggerPresenterToInteractorProtocol: AnyObject {

}

class NetworkLoggerInteractor {

    // MARK: Properties
    weak var presenter: NetworkLoggerInteractorToPresenterProtocol!

}

// MARK: Extension - NetworkLoggerPresenterToInteractorProtocol
extension NetworkLoggerInteractor: NetworkLoggerPresenterToInteractorProtocol {
    
}