//
//  NetworkLoggerRouter.swift
//  NetworkLogger
//
//  Created by macBook on 21.06.2023.
//  
//

import Foundation

// MARK: Protocol - NetworkLoggerPresenterToRouterProtocol (Presenter -> Router)
protocol NetworkLoggerPresenterToRouterProtocol: AnyObject {

}

class NetworkLoggerRouter {

    // MARK: Properties
    weak var view: NetworkLoggerRouterToViewProtocol!
}

// MARK: Extension - NetworkLoggerPresenterToRouterProtocol
extension NetworkLoggerRouter: NetworkLoggerPresenterToRouterProtocol {
    
}