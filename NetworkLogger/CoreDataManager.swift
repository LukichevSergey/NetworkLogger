//
//  CoreDataManager.swift
//  NetworkLogger
//
//  Created by Сергей Лукичев on 21.06.2023.
//

import CoreData

class CoreDataManager {
    
    private let persistentContainer: NSPersistentContainer
    
    init() {
        persistentContainer = NSPersistentContainer(name: "Task")
        persistentContainer.loadPersistentStores { (_, error) in
            if let error = error {
                fatalError("Failed to load Core Data stack: \(error)")
            }
        }
    }
    
//    func create(networkTask: NetworkTask) {
//        let managedObjectContext = persistentContainer.viewContext
//        let networkTaskManagedObject = Task(context: managedObjectContext)
//
//        networkTaskManagedObject.guid = Int16(networkTask.guid)
//        networkTaskManagedObject.status = networkTask.status.rawValue
//        networkTaskManagedObject.request?.createTime = networkTask.request.createTime
//        networkTaskManagedObject.request?.url = networkTask.request.url
//        networkTaskManagedObject.request?.httpMethod = networkTask.request.httpMethod
//        networkTaskManagedObject.request?.headers = try? NSKeyedArchiver.archivedData(withRootObject: networkTask.request.headers, requiringSecureCoding: false)
//        networkTaskManagedObject.request?.body = networkTask.request.body
//
//        if let response = networkTask.response {
//            networkTaskManagedObject.response?.createTime = response.createTime
//            networkTaskManagedObject.response?.headers = try? NSKeyedArchiver.archivedData(withRootObject: response.headers, requiringSecureCoding: false)
//            networkTaskManagedObject.response?.body = response.body
//            networkTaskManagedObject.response?.statusCode = Int16(response.statusCode)
//        }
//
//        do {
//            try managedObjectContext.save()
//        } catch let error {
//            print("Failed to create NetworkTaskManagedObject. Error: \(error)")
//        }
//    }

//    func getAllNetworkTasks() -> [NetworkTask] {
//        let tasks = fetchAllTasks()
//
//        return tasks.compactMap { task in
//
//            let guid = task.guid ?? 0
//            let status = NetworkTask.Status(rawValue: task.status ?? "") ?? .pending
//
//            let request: NetworkTask.Request = {
//                return .init(createTime: task.request?.createTime ?? Date(),
//                             url: task.request?.url ?? "",
//                             httpMethod: task.request?.httpMethod ?? "",
//                             headers: try? NSKeyedArchiver.archivedData(withRootObject: task.request?.headers, requiringSecureCoding: false),
//                             body: task.request?.body)
//            }()
//
//            let response: NetworkTask.Response = {
//                return .init(createTime: task.response?.createTime ?? Date(),
//                             headers: task.response?.headers,
//                             body: task.response?.body,
//                             statusCode: Int(task.response?.statusCode ?? 0))
//            }()
//
//            return NetworkTask(guid: guid, request: request, response: response, status: status)
//        }
//    }
    
//    private func fetchAllTasks() -> [Task] {
//        let managedObjectContext = persistentContainer.viewContext
//        let fetchRequest: NSFetchRequest<Task> = Task.fetchRequest()
//
//        do {
//            let tasks = try managedObjectContext.fetch(fetchRequest)
//            return tasks
//        } catch {
//            print("Failed to fetch tasks. Error: \(error)")
//            return []
//        }
//    }
}
